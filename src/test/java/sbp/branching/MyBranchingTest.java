package sbp.branching;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import sbp.common.Utils;

import static org.mockito.ArgumentMatchers.anyString;

public class MyBranchingTest {

	/**
	 * Проверка сценария, когда замоканный метод .utilFunc2() внешнего класса Utils
	 * возващает true.
	 * В этом случае .maxInt() доджен 0 при любых параметрах
	 */
	@Test
	public void maxIntTestMustReturnZero() {
		Utils utilsMock = Mockito.mock(Utils.class);
		Mockito.when(utilsMock.utilFunc2()).thenReturn(true);

		MyBranching MyBranch = new MyBranching(utilsMock);
		int i1=1;
		int i2=10;
		Assertions.assertEquals(0,MyBranch.maxInt(i1,i2));
	}

	/**
	 * Проверка сценария, когда замоканный метод .utilFunc2() внешнего класса Utils
	 * возващает false.
	 * В этом случае .maxInt() должен возвращать максимальный из двух входящих параметрах
	 * Проверка производится для пар входных параметров:
	 * {1,10} {-12,0} {-12,22}
	 */
	@Test
	public void maxIntTestMustReturnMaxNumber() {
		int i1;
		int i2;
		Utils utilsMock = Mockito.mock(Utils.class);
		Mockito.when(utilsMock.utilFunc2()).thenReturn(false);

		MyBranching MyBranch = new MyBranching(utilsMock);

		i1=1;
		i2=10;
		Assertions.assertEquals(10,MyBranch.maxInt(i1,i2));
		i1=-12;
		i2=0;
		Assertions.assertEquals(0,MyBranch.maxInt(i1,i2));
		i1=-12;
		i2=-22;
		Assertions.assertEquals(-12,MyBranch.maxInt(i1,i2));

	}

	/**
	 * Проверяет результат выполнения метода .ifElseExample()
	 * если замоканный метод .utilFunc2() внешнего объекта класса Utils возвращает true
	 * В этом случае .ifElseExample() должен возвращать true
	 */
	@Test
	public void ifElseExampleTestMustReturnTrue() {
		Utils utilsMock = Mockito.mock(Utils.class);
		Mockito.when(utilsMock.utilFunc2()).thenReturn(true);

		MyBranching MyBranch = new MyBranching(utilsMock);
		Assertions.assertTrue(MyBranch.ifElseExample());
	}

	/**
	 * Проверяет результат выполнения метода .ifElseExample()
	 * если замоканный метод .utilFunc2() внешнего объекта класса Utils возвращает false
	 * В этом случае .ifElseExample() должен возвращать false
	 */
	@Test
	public void ifElseExampleTestMustReturnFalse() {
		Utils utilsMock = Mockito.mock(Utils.class);
		Mockito.when(utilsMock.utilFunc2()).thenReturn(false);

		MyBranching MyBranch = new MyBranching(utilsMock);
		Assertions.assertFalse(MyBranch.ifElseExample());
	}

	/**
	 * Проверка количества обращений к замоканным методам .utilFunc2() и .utilFunc1(anyString()) внешнего объекта
	 * класса Utils
	 * Если метод .utilFunc2() всегда возвращает true и значением входного параметра равным 0
	 * то .utilFunc2() и .utilFunc1(anyString()) должны сработать по одному разу
	 */
	@Test
	public void switchExampleTestIfIEq0ThenRunFunc2AndFunc1() {
		Utils utilsMock = Mockito.mock(Utils.class);
		Mockito.when(utilsMock.utilFunc2()).thenReturn(true);

		MyBranching MyBranch = new MyBranching(utilsMock);
		int i = 0;
		MyBranch.switchExample(i);
		Mockito.verify(utilsMock, Mockito.times(1)).utilFunc2();
		Mockito.verify(utilsMock, Mockito.times(1)).utilFunc1(anyString());
	}

	/**
	 * Проверка количества обращений к замоканным методам .utilFunc2() и .utilFunc1(anyString()) внешнего объекта
	 * класса Utils
	 * При любой реализации метода .utilFunc2() и значением входного параметра равным 1
	 * то .utilFunc2() и .utilFunc1(anyString()) должны сработать по одному разу
	 */
	@Test
	public void switchExampleTestIfIEq1ThenRunFunc2AndFunc1() {
		Utils utilsMock = Mockito.mock(Utils.class);

		MyBranching MyBranch = new MyBranching(utilsMock);
		int i = 1;
		MyBranch.switchExample(i);
		Mockito.verify(utilsMock, Mockito.times(1)).utilFunc2();
		Mockito.verify(utilsMock, Mockito.times(1)).utilFunc1(anyString());
	}

	/**
	 * Проверка количества обращений к замоканным методам .utilFunc2() и .utilFunc1(anyString()) внешнего объекта
	 * класса Utils
	 * При любой реализации метода .utilFunc2() и значением входного параметра равным 2
	 * то .utilFunc2() должен сработать один раз, а .utilFunc1(anyString()) ни разу
	 */
	@Test
	public void switchExampleTestIfIEq2ThenRunFunc2Only() {
		Utils utilsMock = Mockito.mock(Utils.class);

		MyBranching MyBranch = new MyBranching(utilsMock);
		int i = 2;
		MyBranch.switchExample(i);
		Mockito.verify(utilsMock, Mockito.times(1)).utilFunc2();
		Mockito.verify(utilsMock, Mockito.times(0)).utilFunc1(anyString());
	}
}
